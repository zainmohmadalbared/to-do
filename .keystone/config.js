"use strict";
var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __copyProps = (to, from, except, desc) => {
  if (from && typeof from === "object" || typeof from === "function") {
    for (let key of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to, key) && key !== except)
        __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  }
  return to;
};
var __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps(
  // If the importer is in node compatibility mode or this is not an ESM
  // file that has been converted to a CommonJS file using a Babel-
  // compatible transform (i.e. "__esModule" has not been set), then set
  // "default" to the CommonJS "module.exports" for node compatibility.
  isNodeMode || !mod || !mod.__esModule ? __defProp(target, "default", { value: mod, enumerable: true }) : target,
  mod
));
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: true }), mod);

// keystone.ts
var keystone_exports = {};
__export(keystone_exports, {
  default: () => keystone_default
});
module.exports = __toCommonJS(keystone_exports);
var import_core6 = require("@keystone-6/core");
var import_config = require("dotenv/config");

// auth.ts
var import_auth = require("@keystone-6/auth");
var import_session = require("@keystone-6/core/session");

// lib/utils/mail.ts
var import_nodemailer = __toESM(require("nodemailer"));
var transporter = import_nodemailer.default.createTransport({
  host: process.env["NODEMAILER_HOST"],
  port: 587,
  secure: false,
  auth: {
    user: process.env["INVITATION_EMAIL"],
    pass: process.env["INVITATION_PASSWORD"]
  }
});
async function sendPasswordResetEmail(to, token) {
  try {
    console.log({ token });
    const resetUrl = `https://your-frontend-url/reset-password?token=${token}`;
    const mailOptions = {
      from: `Your App Name <${process.env["INVITATION_EMAIL"]}>`,
      to,
      subject: "Password Reset",
      text: `Please use the following link to reset your password: ${resetUrl}`,
      html: `<p>Please use the following link to reset your password:</p><p><a href="${resetUrl}">${resetUrl}</a></p>`
    };
    await transporter.sendMail(mailOptions);
  } catch (error) {
    console.error("Error sending email:", error);
  }
}

// auth.ts
var sessionSecret = process.env.SESSION_SECRET || "my-secret-key-123457890123456789";
var sessionMaxAge = 60 * 60 * 24 * 30;
var { withAuth } = (0, import_auth.createAuth)({
  listKey: "User",
  identityField: "email",
  secretField: "password",
  sessionData: "id name email isAdmin",
  initFirstItem: {
    fields: ["name", "email", "password", "isAdmin"],
    itemData: {
      isAdmin: true
    },
    skipKeystoneWelcome: true
  },
  passwordResetLink: {
    async sendToken(args) {
      sendPasswordResetEmail(args.identity, args.token);
    }
  }
});
var session = (0, import_session.statelessSessions)({
  maxAge: sessionMaxAge,
  secret: sessionSecret
});

// lib/tasks/task.schema.ts
var import_core = require("@keystone-6/core");
var import_fields = require("@keystone-6/core/fields");

// lib/utils/check-is-login.ts
var isLogIn = (context) => {
  return !!context.session?.data.id;
};

// lib/utils/is-admin.ts
var checkAdmin = (context) => {
  return !!context.session?.data?.isAdmin;
};
var isAdmin = (context) => {
  return context.session?.data?.isAdmin;
};

// lib/tasks/accesses/can-create.check.ts
var canCreate = async (context) => {
  if (!isLogIn(context))
    return false;
  const req = context.req;
  const assigneeId = req.body.variables.data.assignee.connect.id;
  const collectionId = req.body.variables.data.collection.connect.id;
  if (!collectionId || !assigneeId)
    return false;
  const collection = await context.query.Collection.findOne({
    where: { id: collectionId },
    query: "id access { user { id } canCreate canRead canUpdate }"
  });
  if (!collection)
    return false;
  const hasPermission = collection.access.some(
    (accessEntry) => accessEntry.user.id === context.session?.data.id && accessEntry.canCreate
  );
  if (!hasPermission)
    return false;
  if (assigneeId) {
    const assigneeHasAccess = collection.access.some(
      (accessEntry) => accessEntry.user.id === assigneeId && accessEntry.canRead && accessEntry.canUpdate
    );
    if (!assigneeHasAccess)
      return false;
  }
  return true;
};

// lib/tasks/accesses/can-update.check.ts
var canUpdate = async (context) => {
  if (!isLogIn(context))
    return false;
  const req = context.req;
  const taskId = req.body.variables.id;
  if (!taskId)
    return false;
  const existingTask = await context.query.Task.findOne({
    where: { id: taskId },
    query: "collection { id access { user { id } canUpdate } }"
  });
  if (!existingTask || !existingTask.collection)
    return false;
  const hasUpdatePermission = existingTask.collection.access.some(
    (accessEntry) => accessEntry.user.id === context.session?.data.id && accessEntry.canUpdate
  );
  if (!hasUpdatePermission)
    return false;
  return true;
};

// lib/tasks/task.schema.ts
var Task = (0, import_core.list)({
  access: {
    operation: {
      create: async ({ context }) => {
        return await canCreate(context);
      },
      query: () => true,
      update: async ({ context }) => {
        return await canUpdate(context);
      },
      delete: () => false
    }
  },
  fields: {
    title: (0, import_fields.text)({
      validation: { isRequired: true },
      ui: { displayMode: "textarea" }
    }),
    description: (0, import_fields.text)({
      validation: { isRequired: true },
      ui: { displayMode: "textarea" }
    }),
    dueDate: (0, import_fields.timestamp)({}),
    priority: (0, import_fields.select)({
      options: [
        { label: "Highest", value: "highest" },
        { label: "High", value: "high" },
        { label: "Medium", value: "medium" },
        { label: "Low", value: "low" },
        { label: "Lowest", value: "lowest" }
      ],
      defaultValue: "medium"
    }),
    status: (0, import_fields.select)({
      options: [
        { label: "To-Do", value: "to-do" },
        { label: "Pending", value: "pending" },
        { label: "Completed", value: "completed" }
      ],
      defaultValue: "to-do"
    }),
    updatedAt: (0, import_fields.timestamp)({
      validation: { isRequired: false },
      access: {
        create: () => false,
        update: () => false,
        read: () => true
      },
      ui: {
        createView: { fieldMode: "hidden" },
        itemView: { fieldMode: "hidden" },
        listView: { fieldMode: "read" }
      }
    }),
    createAt: (0, import_fields.timestamp)({
      validation: { isRequired: true },
      defaultValue: { kind: "now" },
      access: {
        create: () => false,
        update: () => false,
        read: () => true
      },
      ui: {
        createView: { fieldMode: "hidden" },
        itemView: { fieldMode: "hidden" },
        listView: { fieldMode: "read" }
      }
    }),
    owner: (0, import_fields.relationship)({
      ref: "User.owner_tasks",
      access: {
        read: () => true,
        create: () => false,
        update: () => false
      },
      ui: {
        createView: { fieldMode: "hidden" },
        itemView: { fieldMode: "read" },
        listView: { fieldMode: "read" },
        displayMode: "select",
        labelField: "name"
      }
    }),
    assignee: (0, import_fields.relationship)({
      ref: "User.assignee_tasks",
      ui: {
        displayMode: "select",
        labelField: "name"
      }
    }),
    updateBy: (0, import_fields.relationship)({
      ref: "User.update_tasks",
      ui: {
        displayMode: "select",
        labelField: "name",
        createView: { fieldMode: "hidden" },
        itemView: { fieldMode: "hidden" },
        listView: { fieldMode: "read" }
      }
    }),
    collection: (0, import_fields.relationship)({
      ref: "Collection.tasks",
      ui: {
        displayMode: "select",
        labelField: "name"
      }
    })
  },
  hooks: {
    validateInput: async ({ operation, resolvedData, context }) => {
      if (operation === "create")
        resolvedData.owner = { connect: { id: context.session?.data.id } };
      if (operation === "update") {
        resolvedData.updateBy = { connect: { id: context.session?.data.id } };
        resolvedData.updatedAt = (/* @__PURE__ */ new Date()).toISOString();
      }
    }
  },
  ui: {
    listView: {
      initialColumns: ["description", "priority", "status", "dueDate", "user"]
    }
  }
});

// lib/users/user.schema.ts
var import_core2 = require("@keystone-6/core");
var import_fields2 = require("@keystone-6/core/fields");

// lib/users/accesses/can-update.check.ts
var canUpdate2 = (context) => {
  if (!isLogIn(context))
    return false;
  if (isAdmin(context))
    return true;
  const req = context.req;
  if (!req.body.variables.id)
    return false;
  return context.session?.data.id === req.body.variables.id;
};

// lib/users/accesses/can-delete.check.ts
var canDelete = (context) => {
  if (!isLogIn(context))
    return false;
  if (isAdmin(context))
    return true;
  const req = context.req;
  if (!req.body.variables.id)
    return false;
  return context.session?.data.id === req.body.variables.id;
};

// lib/users/user.schema.ts
var User = (0, import_core2.list)({
  access: {
    operation: {
      create: () => true,
      query: ({ context }) => {
        return isLogIn(context);
      },
      update: ({ context }) => {
        return canUpdate2(context);
      },
      delete: ({ context }) => {
        return canDelete(context);
      }
    }
  },
  fields: {
    name: (0, import_fields2.text)({
      validation: { isRequired: true },
      ui: {
        createView: { fieldMode: "edit" },
        itemView: { fieldMode: "edit" },
        listView: { fieldMode: "read" }
      }
    }),
    email: (0, import_fields2.text)({
      validation: { isRequired: true },
      isIndexed: "unique",
      ui: {
        createView: { fieldMode: "edit" },
        itemView: { fieldMode: "edit" },
        listView: { fieldMode: "read" }
      }
    }),
    password: (0, import_fields2.password)({
      validation: { isRequired: true },
      ui: {
        createView: { fieldMode: "edit" },
        itemView: { fieldMode: "hidden" },
        listView: { fieldMode: "hidden" }
      }
    }),
    isAdmin: (0, import_fields2.checkbox)({
      defaultValue: false,
      access: {
        create: ({ context }) => {
          return checkAdmin(context);
        },
        update: ({ context }) => {
          return checkAdmin(context);
        }
      },
      ui: {
        createView: { fieldMode: "edit" },
        itemView: { fieldMode: "edit" },
        listView: { fieldMode: "read" }
      }
    }),
    createAt: (0, import_fields2.timestamp)({
      validation: { isRequired: true },
      defaultValue: { kind: "now" },
      access: {
        create: () => false,
        update: () => false,
        read: () => true
      },
      ui: {
        createView: { fieldMode: "hidden" },
        itemView: { fieldMode: "hidden" },
        listView: { fieldMode: "read" }
      }
    }),
    updatedAt: (0, import_fields2.timestamp)({
      validation: { isRequired: false },
      access: {
        create: () => false,
        update: () => false,
        read: () => true
      },
      ui: {
        createView: { fieldMode: "hidden" },
        itemView: { fieldMode: "hidden" },
        listView: { fieldMode: "read" }
      }
    }),
    invitations: (0, import_fields2.relationship)({
      ref: "Invitation.user",
      ui: {
        createView: { fieldMode: "hidden" },
        itemView: { fieldMode: "edit" },
        listView: { fieldMode: "read" }
      }
    }),
    owner_tasks: (0, import_fields2.relationship)({
      ref: "Task.owner",
      ui: {
        createView: { fieldMode: "hidden" },
        itemView: { fieldMode: "edit" },
        listView: { fieldMode: "read" }
      }
    }),
    update_tasks: (0, import_fields2.relationship)({
      ref: "Task.updateBy",
      ui: {
        createView: { fieldMode: "hidden" },
        itemView: { fieldMode: "edit" },
        listView: { fieldMode: "read" }
      }
    }),
    assignee_tasks: (0, import_fields2.relationship)({
      ref: "Task.assignee",
      ui: {
        createView: { fieldMode: "hidden" },
        itemView: { fieldMode: "edit" },
        listView: { fieldMode: "read" }
      }
    }),
    collections: (0, import_fields2.relationship)({
      ref: "Collection.user",
      ui: {
        createView: { fieldMode: "hidden" },
        itemView: { fieldMode: "edit" },
        listView: { fieldMode: "read" }
      }
    })
  },
  hooks: {
    validateInput: async ({ operation, resolvedData }) => {
      if (operation === "update") {
        resolvedData.updatedAt = (/* @__PURE__ */ new Date()).toISOString();
      }
    }
  }
});

// lib/collections/collection.schema.ts
var import_core3 = require("@keystone-6/core");
var import_fields3 = require("@keystone-6/core/fields");

// lib/collections/accesses/can-update.check.ts
var canUpdateCollection = async (context) => {
  if (!isLogIn(context))
    return false;
  if (isAdmin(context))
    return true;
  const req = context.req;
  const collectionId = req.body.variables.id;
  if (!collectionId)
    return false;
  try {
    const collection = await context.query.Collection.findOne({
      where: { id: collectionId },
      query: "id user { id }"
    });
    if (collection && collection.user.id)
      return context.session?.data.id === collection.user.id;
    return false;
  } catch (error) {
    return false;
  }
};
var canUpdateAccess = async (context) => {
  if (!isLogIn(context))
    return false;
  const req = context.req;
  const accessId = req.body.variables.id;
  if (!accessId)
    return false;
  try {
    const access = await context.query.Access.findOne({
      where: { id: accessId },
      query: "id user { id } collection {id user { id }}  "
    });
    if (access && access.collection.user.id)
      return context.session?.data.id === access.collection.user.id;
    return false;
  } catch (error) {
    return false;
  }
};

// lib/collections/accesses/can-delete.check.ts
var canDeleteCollection = async (context) => {
  if (!isLogIn(context))
    return false;
  if (isAdmin(context))
    return true;
  const req = context.req;
  const collectionId = req.body.variables.id;
  if (!collectionId)
    return false;
  try {
    const collection = await context.query.Collection.findOne({
      where: { id: collectionId },
      query: "id user { id }"
    });
    if (collection && collection.user.id)
      return context.session?.data.id === collection.user.id;
    return false;
  } catch (error) {
    return false;
  }
};
var canDeleteAccess = async (context) => {
  if (!isLogIn(context))
    return false;
  const req = context.req;
  const accessId = req?.body.variables.id;
  if (!accessId)
    return false;
  try {
    const access = await context.query.Access.findOne({
      where: { id: accessId },
      query: "id user { id } collection {id user { id }}  "
    });
    if (access && access.collection.user.id)
      return context.session?.data.id === access.collection.user.id;
    return false;
  } catch (error) {
    return false;
  }
};

// lib/collections/collection.schema.ts
var Collection = (0, import_core3.list)({
  access: {
    operation: {
      create: ({ context }) => {
        return isLogIn(context);
      },
      query: ({ context }) => {
        return isLogIn(context);
      },
      update: async ({ context }) => {
        return await canUpdateCollection(context);
      },
      delete: ({ context }) => {
        return canDeleteCollection(context);
      }
    }
  },
  fields: {
    name: (0, import_fields3.text)({
      validation: { isRequired: true }
    }),
    tasks: (0, import_fields3.relationship)({
      ref: "Task.collection",
      many: true,
      ui: {
        createView: { fieldMode: "hidden" },
        itemView: { fieldMode: "edit" },
        listView: { fieldMode: "read" }
      }
    }),
    access: (0, import_fields3.relationship)({
      ref: "Access.collection",
      many: true,
      ui: {
        createView: { fieldMode: "hidden" },
        itemView: { fieldMode: "edit" },
        listView: { fieldMode: "read" }
      }
    }),
    invitations: (0, import_fields3.relationship)({
      ref: "Invitation.collection",
      many: true,
      ui: {
        createView: { fieldMode: "hidden" },
        itemView: { fieldMode: "edit" },
        listView: { fieldMode: "read" }
      }
    }),
    user: (0, import_fields3.relationship)({
      access: {
        read: () => true,
        create: () => false,
        update: () => false
      },
      ref: "User.collections",
      ui: {
        displayMode: "select",
        labelField: "name",
        createView: { fieldMode: "hidden" },
        itemView: { fieldMode: "edit" },
        listView: { fieldMode: "read" }
      }
    })
  },
  hooks: {
    validateInput: async ({ operation, inputData, resolvedData, context }) => {
      if (operation === "create" && !inputData.user) {
        resolvedData.user = { connect: { id: context.session?.data.id } };
      }
    },
    afterOperation: async ({ operation, item, context }) => {
      if (operation === "create") {
        context.session.data.system = true;
        const userId = item.userId;
        if (userId) {
          await context.db.Access.createOne({
            data: {
              user: { connect: { id: userId } },
              collection: { connect: { id: item.id } },
              canRead: true,
              canUpdate: true,
              canCreate: true
            }
          });
        }
      }
    }
  },
  ui: {
    listView: {
      initialColumns: ["name", "tasks"]
    }
  }
});

// lib/collections/collection-access.schema.ts
var import_core4 = require("@keystone-6/core");
var import_fields4 = require("@keystone-6/core/fields");
var Access = (0, import_core4.list)({
  access: {
    operation: {
      create: ({ context }) => {
        if (!context.session.data.system || !isLogIn(context))
          return false;
        return true;
      },
      query: ({ context }) => {
        return isLogIn(context);
      },
      update: async ({ context }) => {
        return await canUpdateAccess(context);
      },
      delete: async ({ context }) => {
        return await canDeleteAccess(context);
      }
    }
  },
  fields: {
    user: (0, import_fields4.relationship)({
      ref: "User",
      many: false
    }),
    collection: (0, import_fields4.relationship)({
      ref: "Collection.access",
      many: false
    }),
    canRead: (0, import_fields4.checkbox)({ defaultValue: true }),
    canUpdate: (0, import_fields4.checkbox)({ defaultValue: false }),
    canCreate: (0, import_fields4.checkbox)({ defaultValue: false })
  }
});

// lib/invitations/invitation.schema.ts
var import_core5 = require("@keystone-6/core");
var import_fields5 = require("@keystone-6/core/fields");

// lib/invitations/accesses/can-create.check.ts
var canCreate2 = async (context) => {
  if (!isLogIn(context))
    return false;
  if (isAdmin(context))
    return true;
  const req = context.req;
  const collectionId = req.body.variables.data.collection.connect.id;
  const userId = req.body.variables.data.user.connect.id;
  if (!collectionId)
    return false;
  const existingInvitations = await context.query.Invitation.findMany({
    where: {
      collection: { id: { equals: collectionId } },
      user: {
        id: {
          equals: userId
        }
      }
    },
    query: "id"
  });
  if (existingInvitations.length > 0)
    return false;
  const collection = await context.query.Collection.findOne({
    where: {
      id: collectionId
    },
    query: "id user {id}"
  });
  if (collection && collection.user.id)
    return context.session?.data.id === collection.user.id;
  return false;
};

// lib/invitations/accesses/can-update.check.ts
var canUpdate3 = async (context) => {
  if (!isLogIn(context))
    return false;
  if (isAdmin(context))
    return true;
  const req = context.req;
  const invitation = await context.query.Invitation.findOne({
    where: { id: req.body.variables.id },
    query: " id user { id }"
  });
  if (!invitation)
    return false;
  return invitation.user.id === context.session?.data.id;
};

// lib/invitations/invitation.schema.ts
var Invitation = (0, import_core5.list)({
  access: {
    operation: {
      create: async ({ context }) => {
        return await canCreate2(context);
      },
      query: ({ session: session2 }) => !!session2?.data.id,
      update: async ({ context }) => {
        return await canUpdate3(context);
      },
      delete: () => false
    }
  },
  fields: {
    collection: (0, import_fields5.relationship)({
      ref: "Collection.invitations",
      many: false
    }),
    user: (0, import_fields5.relationship)({
      ref: "User.invitations",
      many: false
    }),
    status: (0, import_fields5.select)({
      ui: {
        createView: { fieldMode: "hidden" },
        itemView: { fieldMode: "edit" },
        listView: { fieldMode: "read" }
      },
      options: [
        { label: "Pending", value: "pending" },
        { label: "Accepted", value: "accepted" },
        { label: "Declined", value: "declined" }
      ],
      defaultValue: "pending",
      access: {
        create: () => false
      },
      validation: { isRequired: true }
    }),
    message: (0, import_fields5.text)({
      ui: {
        displayMode: "textarea"
      }
    }),
    createAt: (0, import_fields5.timestamp)({
      validation: { isRequired: true },
      defaultValue: { kind: "now" },
      access: {
        create: () => false,
        update: () => false,
        read: () => true
      },
      ui: {
        createView: { fieldMode: "hidden" },
        itemView: { fieldMode: "hidden" },
        listView: { fieldMode: "read" }
      }
    }),
    updatedAt: (0, import_fields5.timestamp)({
      validation: { isRequired: false },
      access: {
        create: () => false,
        update: () => false,
        read: () => true
      },
      ui: {
        createView: { fieldMode: "hidden" },
        itemView: { fieldMode: "hidden" },
        listView: { fieldMode: "read" }
      }
    })
  },
  hooks: {
    validateInput: async ({ operation, resolvedData }) => {
      if (operation === "update") {
        resolvedData.updatedAt = (/* @__PURE__ */ new Date()).toISOString();
      }
    },
    afterOperation: async ({ operation, item, context, resolvedData }) => {
      if (operation === "update") {
        if (item.status === "accepted") {
          const userId = item.userId;
          context.session.data.system = true;
          if (userId) {
            await context.db.Access.createOne({
              data: {
                user: { connect: { id: userId } },
                collection: { connect: { id: item.collectionId } },
                canRead: true,
                canUpdate: false,
                canCreate: false
              }
            });
          }
        }
      }
    }
  },
  ui: {
    listView: {
      initialColumns: ["collection", "user", "status", "createdAt"]
    }
  }
});

// keystone.ts
var databaseUrl = process.env.DB_URL;
var keystone_default = withAuth(
  (0, import_core6.config)({
    db: {
      provider: "postgresql",
      url: databaseUrl
    },
    lists: { Task, User, Collection, Access, Invitation },
    session,
    ui: {
      isAccessAllowed: checkAdmin
    }
  })
);
//# sourceMappingURL=config.js.map
