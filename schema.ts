import { list } from '@keystone-6/core';
import { allowAll } from '@keystone-6/core/access';
import {
	text,
	checkbox,
	relationship,
	password,
	timestamp,
	select,
} from '@keystone-6/core/fields';

import { document } from '@keystone-6/fields-document';

import type { Lists } from '.keystone/types';
import { adminOnlyFieldAccess } from './lib/users/is-admin.access';

export const lists: Lists = {};
