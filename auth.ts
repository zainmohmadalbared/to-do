// auth.ts
import { createAuth } from '@keystone-6/auth';
import { statelessSessions } from '@keystone-6/core/session';
import { sendPasswordResetEmail } from './lib/utils/mail'; // Custom email function

const sessionSecret =
	process.env.SESSION_SECRET || 'my-secret-key-123457890123456789';
const sessionMaxAge = 60 * 60 * 24 * 30; // 30 days

export const { withAuth } = createAuth({
	listKey: 'User',
	identityField: 'email',
	secretField: 'password',
	sessionData: 'id name email isAdmin',
	initFirstItem: {
		fields: ['name', 'email', 'password', 'isAdmin'],
		itemData: {
			isAdmin: true,
		},
		skipKeystoneWelcome: true,
	},
	passwordResetLink: {
		async sendToken(args) {
			 sendPasswordResetEmail(args.identity, args.token);
		},
	},
});

export const session = statelessSessions({
	maxAge: sessionMaxAge,
	secret: sessionSecret,
});
