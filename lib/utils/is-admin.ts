import { KeystoneContext } from '@keystone-6/core/types';

export const checkAdmin = (context: KeystoneContext): boolean => {
	return !!context.session?.data?.isAdmin;
};

export const isAdmin = (context: KeystoneContext): boolean => {
	return context.session?.data?.isAdmin;
};
