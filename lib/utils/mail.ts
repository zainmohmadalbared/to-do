import nodemailer from 'nodemailer';

const transporter = nodemailer.createTransport({
	host: process.env['NODEMAILER_HOST'],
	port: 587,
	secure: false,
	auth: {
		user: process.env['INVITATION_EMAIL'],
		pass: process.env['INVITATION_PASSWORD'],
	},
});


export async function sendPasswordResetEmail(to: string, token: string) {
	try {
		console.log({ token });
		const resetUrl = `https://your-frontend-url/reset-password?token=${token}`;

		const mailOptions = {
			from: `Your App Name <${process.env['INVITATION_EMAIL']}>`,
			to,
			subject: 'Password Reset',
			text: `Please use the following link to reset your password: ${resetUrl}`,
			html: `<p>Please use the following link to reset your password:</p><p><a href="${resetUrl}">${resetUrl}</a></p>`,
		};
		await transporter.sendMail(mailOptions);
	} catch (error) {
		console.error('Error sending email:', error);
	}
}
