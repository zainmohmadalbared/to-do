import { KeystoneContext } from '@keystone-6/core/types';

export const isLogIn = (context: KeystoneContext): boolean => {
	return !!context.session?.data.id;
};
