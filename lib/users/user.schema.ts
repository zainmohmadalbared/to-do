import { list } from '@keystone-6/core';
import {
	text,
	password,
	checkbox,
	relationship,
	timestamp,
} from '@keystone-6/core/fields';
import { checkAdmin, isLogIn } from '../utils';
import { canUpdate } from './accesses/can-update.check';
import { canDelete } from './accesses/can-delete.check';

export const User = list({
	access: {
		operation: {
			create: () => true,
			query: ({ context }) => {
				return isLogIn(context);
			},
			update: ({ context }) => {
				return canUpdate(context);
			},
			delete: ({ context }) => {
				return canDelete(context);
			},
		},
	},
	fields: {
		name: text({
			validation: { isRequired: true },
			ui: {
				createView: { fieldMode: 'edit' },
				itemView: { fieldMode: 'edit' },
				listView: { fieldMode: 'read' },
			},
		}),
		email: text({
			validation: { isRequired: true },
			isIndexed: 'unique',
			ui: {
				createView: { fieldMode: 'edit' },
				itemView: { fieldMode: 'edit' },
				listView: { fieldMode: 'read' },
			},
		}),
		password: password({
			validation: { isRequired: true },
			ui: {
				createView: { fieldMode: 'edit' },
				itemView: { fieldMode: 'hidden' },
				listView: { fieldMode: 'hidden' },
			},
		}),
		isAdmin: checkbox({
			defaultValue: false,
			access: {
				create: ({ context }) => {
					return checkAdmin(context);
				},
				update: ({ context }) => {
					return checkAdmin(context);
				},
			},
			ui: {
				createView: { fieldMode: 'edit' },
				itemView: { fieldMode: 'edit' },
				listView: { fieldMode: 'read' },
			},
		}),
		createAt: timestamp({
			validation: { isRequired: true },
			defaultValue: { kind: 'now' },
			access: {
				create: () => false,
				update: () => false,
				read: () => true,
			},
			ui: {
				createView: { fieldMode: 'hidden' },
				itemView: { fieldMode: 'hidden' },
				listView: { fieldMode: 'read' },
			},
		}),
		updatedAt: timestamp({
			validation: { isRequired: false },
			access: {
				create: () => false,
				update: () => false,
				read: () => true,
			},
			ui: {
				createView: { fieldMode: 'hidden' },
				itemView: { fieldMode: 'hidden' },
				listView: { fieldMode: 'read' },
			},
		}),
		invitations: relationship({
			ref: 'Invitation.user',

			ui: {
				createView: { fieldMode: 'hidden' },
				itemView: { fieldMode: 'edit' },
				listView: { fieldMode: 'read' },
			},
		}),
		owner_tasks: relationship({
			ref: 'Task.owner',

			ui: {
				createView: { fieldMode: 'hidden' },
				itemView: { fieldMode: 'edit' },
				listView: { fieldMode: 'read' },
			},
		}),
		update_tasks: relationship({
			ref: 'Task.updateBy',

			ui: {
				createView: { fieldMode: 'hidden' },
				itemView: { fieldMode: 'edit' },
				listView: { fieldMode: 'read' },
			},
		}),
		assignee_tasks: relationship({
			ref: 'Task.assignee',

			ui: {
				createView: { fieldMode: 'hidden' },
				itemView: { fieldMode: 'edit' },
				listView: { fieldMode: 'read' },
			},
		}),
		collections: relationship({
			ref: 'Collection.user',

			ui: {
				createView: { fieldMode: 'hidden' },
				itemView: { fieldMode: 'edit' },
				listView: { fieldMode: 'read' },
			},
		}),
	},
	hooks: {
		validateInput: async ({ operation, resolvedData }) => {
			if (operation === 'update') {
				resolvedData.updatedAt = new Date().toISOString();
			}
		},
	},
});
