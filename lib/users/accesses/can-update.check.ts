import { KeystoneContext } from '@keystone-6/core/types';
import { Request } from 'express';
import { isAdmin, isLogIn } from '../../utils';

export const canUpdate = (context: KeystoneContext): boolean => {
	if (!isLogIn(context)) return false;
	if (isAdmin(context)) return true;
	const req = context.req as Request;
	if (!req.body.variables.id) return false;
	return context.session?.data.id === req.body.variables.id;
};
