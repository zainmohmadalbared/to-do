import { list } from '@keystone-6/core';
import { text, select, relationship, timestamp } from '@keystone-6/core/fields';
import { canCreate } from './accesses/can-create.check';
import { canUpdate } from './accesses/can-update.check';
export const Invitation = list({
	access: {
		operation: {
			create: async ({ context }) => {
				return await canCreate(context);
			},
			query: ({ session }) => !!session?.data.id,
			update: async ({ context }) => {
				return await canUpdate(context);
			},
			delete: () => false,
		},
	},
	fields: {
		collection: relationship({
			ref: 'Collection.invitations',
			many: false,
		}),
		user: relationship({
			ref: 'User.invitations',
			many: false,
		}),
		status: select({
			ui: {
				createView: { fieldMode: 'hidden' },
				itemView: { fieldMode: 'edit' },
				listView: { fieldMode: 'read' },
			},
			options: [
				{ label: 'Pending', value: 'pending' },
				{ label: 'Accepted', value: 'accepted' },
				{ label: 'Declined', value: 'declined' },
			],
			defaultValue: 'pending',
			access: {
				create: () => false,
			},
			validation: { isRequired: true },
		}),
		message: text({
			ui: {
				displayMode: 'textarea',
			},
		}),
		createAt: timestamp({
			validation: { isRequired: true },
			defaultValue: { kind: 'now' },
			access: {
				create: () => false,
				update: () => false,
				read: () => true,
			},
			ui: {
				createView: { fieldMode: 'hidden' },
				itemView: { fieldMode: 'hidden' },
				listView: { fieldMode: 'read' },
			},
		}),
		updatedAt: timestamp({
			validation: { isRequired: false },
			access: {
				create: () => false,
				update: () => false,
				read: () => true,
			},
			ui: {
				createView: { fieldMode: 'hidden' },
				itemView: { fieldMode: 'hidden' },
				listView: { fieldMode: 'read' },
			},
		}),
	},
	hooks: {
		validateInput: async ({ operation, resolvedData }) => {
			if (operation === 'update') {
				resolvedData.updatedAt = new Date().toISOString();
			}
		},
		afterOperation: async ({ operation, item, context, resolvedData }) => {
			if (operation === 'update') {
				if (item.status === 'accepted') {
					const userId = item.userId;
					context.session.data.system = true;
					if (userId) {
						await context.db.Access.createOne({
							data: {
								user: { connect: { id: userId } },
								collection: { connect: { id: item.collectionId } },
								canRead: true,
								canUpdate: false,
								canCreate: false,
							},
						});
					}
				}
			}
		},
	},
	ui: {
		listView: {
			initialColumns: ['collection', 'user', 'status', 'createdAt'],
		},
	},
});
