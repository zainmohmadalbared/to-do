import { KeystoneContext } from '@keystone-6/core/types';
import { isAdmin, isLogIn } from '../../utils';
import { Request } from 'express';

export const canCreate = async (context: KeystoneContext): Promise<boolean> => {
	if (!isLogIn(context)) return false;
	if (isAdmin(context)) return true;
	const req = context.req as Request;

	const collectionId = req.body.variables.data.collection.connect.id;
	const userId = req.body.variables.data.user.connect.id;
	if (!collectionId) return false;
	const existingInvitations = await context.query.Invitation.findMany({
		where: {
			collection: { id: { equals: collectionId } },
			user: {
				id: {
					equals: userId,
				},
			},
		},
		query: 'id',
	});

	if (existingInvitations.length > 0) return false;
	const collection = await context.query.Collection.findOne({
		where: {
			id: collectionId,
		},

		query: 'id user {id}',
	});
	if (collection && collection.user.id)
		return context.session?.data.id === collection.user.id;
	return false;
};
