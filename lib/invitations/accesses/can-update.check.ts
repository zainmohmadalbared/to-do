import { KeystoneContext } from '@keystone-6/core/types';
import { isAdmin, isLogIn } from '../../utils';
import { Request } from 'express';

export const canUpdate = async (context: KeystoneContext): Promise<boolean> => {
	if (!isLogIn(context)) return false;
	if (isAdmin(context)) return true;
	const req = context.req as Request;

	const invitation = await context.query.Invitation.findOne({
		where: { id: req.body.variables.id },
		query: ' id user { id }',
	});
	if (!invitation) return false;

	return invitation.user.id === context.session?.data.id;
};
