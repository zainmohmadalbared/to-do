import { KeystoneContext } from '@keystone-6/core/types';
import { isAdmin, isLogIn } from '../../utils';
import { Request } from 'express';

export const canDeleteCollection = async (
	context: KeystoneContext
): Promise<boolean> => {
	if (!isLogIn(context)) return false;
	if (isAdmin(context)) return true;
	const req = context.req as Request;

	const collectionId = req.body.variables.id;

	if (!collectionId) return false;

	try {
		const collection = await context.query.Collection.findOne({
			where: { id: collectionId },
			query: 'id user { id }',
		});
		if (collection && collection.user.id)
			return context.session?.data.id === collection.user.id;

		return false;
	} catch (error) {
		return false;
	}
};

export const canDeleteAccess = async (
	context: KeystoneContext
): Promise<boolean> => {
	if (!isLogIn(context)) return false;
	// if (isAdmin(context)) return true;
	const req = context.req as Request;

	const accessId = req?.body.variables.id;

	if (!accessId) return false;

	try {
		const access = await context.query.Access.findOne({
			where: { id: accessId },
			query: 'id user { id } collection {id user { id }}  ',
		});
		if (access && access.collection.user.id)
			return context.session?.data.id === access.collection.user.id;

		return false;
	} catch (error) {
		return false;
	}
};
