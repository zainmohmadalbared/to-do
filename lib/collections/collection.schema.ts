import { list } from '@keystone-6/core';
import { text, relationship } from '@keystone-6/core/fields';
import { isLogIn } from '../utils';
import { canUpdateCollection } from './accesses/can-update.check';
import { canDeleteCollection } from './accesses/can-delete.check';
export const Collection = list({
	access: {
		operation: {
			create: ({ context }) => {
				return isLogIn(context);
			},
			query: ({ context }) => {
				return isLogIn(context);
			},
			update: async ({ context }) => {
				return await canUpdateCollection(context);
			},
			delete: ({ context }) => {
				return canDeleteCollection(context);
			},
		},
	},
	fields: {
		name: text({
			validation: { isRequired: true },
		}),
		tasks: relationship({
			ref: 'Task.collection',
			many: true,
			ui: {
				createView: { fieldMode: 'hidden' },
				itemView: { fieldMode: 'edit' },
				listView: { fieldMode: 'read' },
			},
		}),
		access: relationship({
			ref: 'Access.collection',
			many: true,
			ui: {
				createView: { fieldMode: 'hidden' },
				itemView: { fieldMode: 'edit' },
				listView: { fieldMode: 'read' },
			},
		}),
		invitations: relationship({
			ref: 'Invitation.collection',
			many: true,
			ui: {
				createView: { fieldMode: 'hidden' },
				itemView: { fieldMode: 'edit' },
				listView: { fieldMode: 'read' },
			},
		}),
		user: relationship({
			access: {
				read: () => true,
				create: () => false,
				update: () => false,
			},
			ref: 'User.collections',
			ui: {
				displayMode: 'select',
				labelField: 'name',
				createView: { fieldMode: 'hidden' },
				itemView: { fieldMode: 'edit' },
				listView: { fieldMode: 'read' },
			},
		}),
	},
	hooks: {
		validateInput: async ({ operation, inputData, resolvedData, context }) => {
			if (operation === 'create' && !inputData.user) {
				resolvedData.user = { connect: { id: context.session?.data.id } };
			}
		},
		afterOperation: async ({ operation, item, context }) => {
			if (operation === 'create') {
				context.session.data.system = true;

				const userId = item.userId;
				if (userId) {
					await context.db.Access.createOne({
						data: {
							user: { connect: { id: userId } },
							collection: { connect: { id: item.id } },
							canRead: true,
							canUpdate: true,
							canCreate: true,
						},
					});
				}
			}
		},
	},
	ui: {
		listView: {
			initialColumns: ['name', 'tasks'],
		},
	},
});
