import { list } from '@keystone-6/core';
import { checkbox, relationship } from '@keystone-6/core/fields';
import { isLogIn } from '../utils';
import { canUpdateAccess } from './accesses/can-update.check';
import { canDeleteAccess } from './accesses/can-delete.check';

export const Access = list({
	access: {
		operation: {
			create: ({ context }) => {
				if (!context.session.data.system || !isLogIn(context)) return false;
				return true;
			},
			query: ({ context }) => {
				return isLogIn(context);
			},
			update: async ({ context }) => {
				return await canUpdateAccess(context);
			},
			delete: async ({ context }) => {
				return await canDeleteAccess(context);
			},
		},
	},
	fields: {
		user: relationship({
			ref: 'User',
			many: false,
		}),
		collection: relationship({
			ref: 'Collection.access',
			many: false,
		}),
		canRead: checkbox({ defaultValue: true }),
		canUpdate: checkbox({ defaultValue: false }),
		canCreate: checkbox({ defaultValue: false }),
	},
});
