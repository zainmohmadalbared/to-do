import { list } from '@keystone-6/core';
import { text, timestamp, select, relationship } from '@keystone-6/core/fields';
import { canCreate } from './accesses/can-create.check';
import { canUpdate } from './accesses/can-update.check';

export const Task = list({
	access: {
		operation: {
			create: async ({ context }) => {
				return await canCreate(context);
			},
			query: () => true,
			update: async ({ context }) => {
				return await canUpdate(context);
			},
			delete: () => false,
		},
	},
	fields: {
		title: text({
			validation: { isRequired: true },
			ui: { displayMode: 'textarea' },
		}),
		description: text({
			validation: { isRequired: true },
			ui: { displayMode: 'textarea' },
		}),
		dueDate: timestamp({}),
		priority: select({
			options: [
				{ label: 'Highest', value: 'highest' },
				{ label: 'High', value: 'high' },
				{ label: 'Medium', value: 'medium' },
				{ label: 'Low', value: 'low' },
				{ label: 'Lowest', value: 'lowest' },
			],
			defaultValue: 'medium',
		}),
		status: select({
			options: [
				{ label: 'To-Do', value: 'to-do' },
				{ label: 'Pending', value: 'pending' },
				{ label: 'Completed', value: 'completed' },
			],
			defaultValue: 'to-do',
		}),
		updatedAt: timestamp({
			validation: { isRequired: false },
			access: {
				create: () => false,
				update: () => false,
				read: () => true,
			},
			ui: {
				createView: { fieldMode: 'hidden' },
				itemView: { fieldMode: 'hidden' },
				listView: { fieldMode: 'read' },
			},
		}),
		createAt: timestamp({
			validation: { isRequired: true },
			defaultValue: { kind: 'now' },
			access: {
				create: () => false,
				update: () => false,
				read: () => true,
			},
			ui: {
				createView: { fieldMode: 'hidden' },
				itemView: { fieldMode: 'hidden' },
				listView: { fieldMode: 'read' },
			},
		}),
		owner: relationship({
			ref: 'User.owner_tasks',
			access: {
				read: () => true,
				create: () => false,
				update: () => false,
			},
			ui: {
				createView: { fieldMode: 'hidden' },
				itemView: { fieldMode: 'read' },
				listView: { fieldMode: 'read' },
				displayMode: 'select',
				labelField: 'name',
			},
		}),
		assignee: relationship({
			ref: 'User.assignee_tasks',
			ui: {
				displayMode: 'select',
				labelField: 'name',
			},
		}),
		updateBy: relationship({
			ref: 'User.update_tasks',
			ui: {
				displayMode: 'select',
				labelField: 'name',
				createView: { fieldMode: 'hidden' },
				itemView: { fieldMode: 'hidden' },
				listView: { fieldMode: 'read' },
			},
		}),
		collection: relationship({
			ref: 'Collection.tasks',
			ui: {
				displayMode: 'select',
				labelField: 'name',
			},
		}),
	},

	hooks: {
		validateInput: async ({ operation, resolvedData, context }) => {
			if (operation === 'create')
				resolvedData.owner = { connect: { id: context.session?.data.id } };

			if (operation === 'update') {
				resolvedData.updateBy = { connect: { id: context.session?.data.id } };
				resolvedData.updatedAt = new Date().toISOString();
			}
		},
	},
	ui: {
		listView: {
			initialColumns: ['description', 'priority', 'status', 'dueDate', 'user'],
		},
	},
});
