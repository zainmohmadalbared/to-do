import { KeystoneContext } from '@keystone-6/core/types';
import { Request } from 'express';
import { isLogIn } from '../../utils';

export const canCreate = async (context: KeystoneContext): Promise<boolean> => {
	if (!isLogIn(context)) return false;

	const req = context.req as Request;

	const assigneeId = req.body.variables.data.assignee.connect.id;
	const collectionId = req.body.variables.data.collection.connect.id;
	if (!collectionId || !assigneeId) return false;

	const collection = await context.query.Collection.findOne({
		where: { id: collectionId },
		query: 'id access { user { id } canCreate canRead canUpdate }',
	});
	if (!collection) return false;

	const hasPermission = collection.access.some(
		(accessEntry: { user: { id: string }; canCreate: boolean }) =>
			accessEntry.user.id === context.session?.data.id && accessEntry.canCreate
	);

	if (!hasPermission) return false;

	if (assigneeId) {
		const assigneeHasAccess = collection.access.some(
			(accessEntry: {
				user: { id: string };
				canRead: boolean;
				canUpdate: boolean;
			}) =>
				accessEntry.user.id === assigneeId &&
				accessEntry.canRead &&
				accessEntry.canUpdate
		);

		if (!assigneeHasAccess) return false;
	}
	return true;
};
