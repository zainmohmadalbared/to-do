import { KeystoneContext } from '@keystone-6/core/types';
import { Request } from 'express';
import { isLogIn } from '../../utils';

export const canUpdate = async (context: KeystoneContext): Promise<boolean> => {
	if (!isLogIn(context)) return false;

	const req = context.req as Request;
	const taskId = req.body.variables.id;
	if (!taskId) return false;

	const existingTask = await context.query.Task.findOne({
		where: { id: taskId },
		query: 'collection { id access { user { id } canUpdate } }',
	});

	if (!existingTask || !existingTask.collection) return false;

	const hasUpdatePermission = existingTask.collection.access.some(
		(accessEntry: { user: { id: string }; canUpdate: boolean }) =>
			accessEntry.user.id === context.session?.data.id && accessEntry.canUpdate
	);

	if (!hasUpdatePermission) return false;

	return true;
};
