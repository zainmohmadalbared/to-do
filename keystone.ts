import { config } from '@keystone-6/core';
import 'dotenv/config';
import { lists } from './schema';
import { withAuth, session } from './auth';

import { Task } from './lib/tasks/task.schema';
import { User } from './lib/users/user.schema';
import { Collection } from './lib/collections/collection.schema';
import { Access } from './lib/collections/collection-access.schema';
import { Invitation } from './lib/invitations/invitation.schema';
import { checkAdmin } from './lib/utils/is-admin';

const databaseUrl = process.env.DB_URL!;

export default withAuth(
	config({
		db: {
			provider: 'postgresql',
			url: databaseUrl,
		},
		lists: { Task, User, Collection, Access, Invitation },
		session,
		ui: {
			isAccessAllowed: checkAdmin,
		},
	})
);
